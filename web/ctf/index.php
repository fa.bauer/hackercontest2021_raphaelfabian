<!doctype html>
<html lang="en">
    <head>
        <title>CTF Flag Checker - ROP Attacks</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="../css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
        <script src="../js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    
    </head>
    <body>
    
    <div class="row d-flex justify-content-center mt-100">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
            <div class="jumbotron">
            <h1 class="display-4">CTF Flag Checker</h1>
            <p class="lead">Here you can check whether you were able to find the correct flags.</p>
            <hr class="my-4">
            <p></p>
            
            
            <form method="post">
            <div class="form-group">
                <input name="t1"  class="form-control" type="text" placeholder="Token #1" aria-describedby="timeHelp" >
                <small id="timeHelp" class="form-text text-muted">Token of the first challenge.</small><br>
            </div>
            <br />
            <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <br />
            <br />            
            <?php

            if (isset($_POST['t1'])) {
                $t1 =  $_POST['t1'];
                if (hash('sha256', $t1) == "5ed9b2f3c3dbc4be5966b379e746affb9dc43571515bc185f251052e71400e33"){
                    echo "Token #1 was correct.";
                }else{
                    echo "Token #1 was either incorrect or not supplied";
                }
            }
            ?>



            </div>
            </div>
        </div>
    </div>
    
    
    
    </div>

    </body>
    

    <style>
    .icon {
        background: no-repeat center url('img/icon.png');
        height: 23px;
        width: 23px;
        display: block;
        /* Other styles here */
    }
    body {
    background-color: #323232/*#f2f7fb*/
    }

    .mt-100 {
        margin-top: 100px
    }

    .card {
        border-radius: 5px;
        -webkit-box-shadow: 0 0 5px 0 rgba(43, 43, 43, .1), 0 11px 6px -7px rgba(43, 43, 43, .1);
        box-shadow: 0 0 5px 0 rgba(43, 43, 43, .1), 0 11px 6px -7px rgba(43, 43, 43, .1);
        border: none;
        margin-bottom: 30px;
        -webkit-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out
    }

    .card .card-header {
        background-color: transparent;
        border-bottom: none;
        padding: 20px;
        position: relative
    }

    .card .card-header h5:after {
        content: "";
        background-color: #d2d2d2;
        width: 101px;
        height: 1px;
        position: absolute;
        bottom: 6px;
        left: 20px
    }

    .card .card-block {
        padding: 1.25rem
    }

    .dropzone.dz-clickable {
        cursor: pointer
    }

    .dropzone {
        min-height: 150px;
        border: 1px solid rgba(42, 42, 42, 0.05);
        background: rgba(204, 204, 204, 0.15);
        padding: 20px;
        border-radius: 5px;
        -webkit-box-shadow: inset 0 0 5px 0 rgba(43, 43, 43, 0.1);
        box-shadow: inset 0 0 5px 0 rgba(43, 43, 43, 0.1)
    }

    .m-t-20 {
        margin-top: 20px
    }

    .btn-primary,
    .sweet-alert button.confirm,
    .wizard>.actions a {
        background-color: #4099ff;
        border-color: #4099ff;
        color: #fff;
        cursor: pointer;
        -webkit-transition: all ease-in .3s;
        transition: all ease-in .3s
    }

    .btn {
        border-radius: 2px;
        text-transform: capitalize;
        font-size: 15px;
        padding: 10px 19px;
        cursor: pointer
    }
    </style>
</html>
