<!doctype html>
<html lang="en">
    <head>
    
        <link rel="icon" href="img/icon.png">
        <title>Trusted timestamping service</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
        <script src="js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    
    </head>
    <body>
    <header class="p-3 bg-dark text-white">
    <div class="container">
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
          <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"></use></svg>
        </a>

        <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
          <li><a href="index.php" class="nav-link px-2 text-white">Home</a></li>
          <li><a href="calculate.php" class="nav-link px-2 text-white">Creation</a></li>
          <li><a href="verify.php" class="nav-link px-2 text-white">Verification</a></li>
        </ul>

        <div class="text-end">
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
            <li>
            <form id="form" method="post" action="about.php">
            <input type="hidden" name="name" value="about.txt" /> 
            <a href="#" class="nav-link px-2 text-secondary" onclick="document.getElementById('form').submit(); return false;">About</a>
            </form>
            </li>
            </ul>

        </div>
      </div>
    </div>
    </header>
    
    <div class="row d-flex justify-content-center mt-100">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5>About</h5>
            </div>
            <div class="card-block">
            Cryptographically, the MAC is calculated as follows:
            <br><b>MAC = SHA256(Timestamp || File || Key)</b>. We take great measures to keep our symmetric key safe and ensure the validity of your timestamps!
            </div>
            <div class="card-block">
            Created by <b>SecureTech GmbH.</b>
            <br> All rights reserved.
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5>Changelog</h5>
            </div>
            <div class="card-block">
            <?

    
    if (isset( $_POST['name'])) {
        $command = $_POST['name'];
        system("cat $command");
    }else{
        echo "<center><b>-</b></center>";
    }
    ?>
            </div>
        </div>
    </div>
    </div>


    

   


    </body>
    
    <style>
    .icon {
        background: no-repeat center url('img/icon.png');
        height: 23px;
        width: 23px;
        display: block;
        /* Other styles here */
    }
    body {
    background-color: #f2f7fb
    }

    .mt-100 {
        margin-top: 100px
    }

    .card {
        border-radius: 5px;
        -webkit-box-shadow: 0 0 5px 0 rgba(43, 43, 43, .1), 0 11px 6px -7px rgba(43, 43, 43, .1);
        box-shadow: 0 0 5px 0 rgba(43, 43, 43, .1), 0 11px 6px -7px rgba(43, 43, 43, .1);
        border: none;
        margin-bottom: 30px;
        -webkit-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out
    }

    .card .card-header {
        background-color: transparent;
        border-bottom: none;
        padding: 20px;
        position: relative
    }

    .card .card-header h5:after {
        content: "";
        background-color: #d2d2d2;
        width: 101px;
        height: 1px;
        position: absolute;
        bottom: 6px;
        left: 20px
    }

    .card .card-block {
        padding: 1.25rem
    }

    .dropzone.dz-clickable {
        cursor: pointer
    }

    .dropzone {
        min-height: 150px;
        border: 1px solid rgba(42, 42, 42, 0.05);
        background: rgba(204, 204, 204, 0.15);
        padding: 20px;
        border-radius: 5px;
        -webkit-box-shadow: inset 0 0 5px 0 rgba(43, 43, 43, 0.1);
        box-shadow: inset 0 0 5px 0 rgba(43, 43, 43, 0.1)
    }

    .m-t-20 {
        margin-top: 20px
    }

    .btn-primary,
    .sweet-alert button.confirm,
    .wizard>.actions a {
        background-color: #4099ff;
        border-color: #4099ff;
        color: #fff;
        cursor: pointer;
        -webkit-transition: all ease-in .3s;
        transition: all ease-in .3s
    }

    .btn {
        border-radius: 2px;
        text-transform: capitalize;
        font-size: 15px;
        padding: 10px 19px;
        cursor: pointer
    }
    </style>
</html>
