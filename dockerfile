FROM php:apache-buster
EXPOSE 8000
RUN useradd -ms /bin/bash user
ADD root /root
ADD user /home/user
ADD web /var/www/html
RUN chown root /home/user/timeStamper
RUN chmod 555 /home/user/timeStamper
RUN chmod u+s /home/user/timeStamper
RUN chown -R root:root /root
RUN chmod -R 400 /root
RUN chown -R www-data:www-data /var/www/
RUN apt-get update
RUN apt-get install -y python3-pip netcat
